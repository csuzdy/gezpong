class Destroyer {

    constructor(width, height) {
        this.engine = Matter.Engine.create();
        this.world = this.engine.world;
        this.world.bodies = [];
        this.world.gravity = {
            x: 0,
            y: 0
        };
        this.width = width;
        this.height = height;
        this.render = Matter.Render.create({
            element: document.body,
            engine: this.engine,
            options: {
                width: this.width,
                height: this.height,
                background: 'none',
                wireframes: false
            }
        });
        Matter.Render.lookAt(this.render, {
            min: { x: 0, y: 0 },
            max: { x: this.width, y: this.height }
        });
        this.runner = Matter.Runner.create();
        this.brickSize = 40;
        this.points = document.getElementById('points');
        this.ended = false;
        this.addFloor();
        this.ball = [];
        this.ballConstraint = [];
    }

    addFloor() {
        let floor = document.getElementById('floor');
        floor.setAttribute("style",`width: ${this.width}px; height: ${this.height}px`);
    }

    start() {
        console.log('Start app');
        this.addBricks();
        this.addWalls();
        this.addBall(0);
        this.addBall(1);
        this.addBrickGC();
        Matter.Render.run(this.render);
        Matter.Runner.run(this.runner, this.engine);
    }

    stop() {
        this.ended = true;
        Matter.Render.stop(this.render);
        Matter.Runner.stop(this.runner);
    }

    addWalls() {
        Matter.World.add(this.world, [
            //bottom
            Matter.Bodies.rectangle(this.width/2, this.height+this.brickSize/2-10, this.width-this.brickSize*4, this.brickSize, {isStatic: true, render: {fillStyle: '#8B4513'}}),
            //top
            Matter.Bodies.rectangle(this.width/2, -this.brickSize/2+10, this.width-this.brickSize*4, this.brickSize, {isStatic: true, render: {fillStyle: '#8B4513'}}),
            //left
            Matter.Bodies.rectangle(-this.brickSize/2+10, this.height/2, this.brickSize, this.height, {isStatic: true}),
            //right
            Matter.Bodies.rectangle(this.width+this.brickSize/2-10, this.height/2, this.brickSize, this.height, {isStatic: true})
        ]);
    }

    addBricks() {
        let brickSize = this.brickSize,
            columns = 10,
            rows = 10,
            boxes = [];
        this.stack = Matter.Composites.stack(this.width-brickSize*columns-brickSize/2, this.height-brickSize*rows-brickSize/2, columns, rows, 0, 0, function(x, y) {
            let rand = Matter.Common.choose(['ball', 'car', 'brick']);
            switch (rand) {
            case 'brick':
                return Matter.Bodies.rectangle(x, y, brickSize, brickSize, {
                    render: {
                        sprite: {
                            xScale: brickSize/160,
                            yScale: brickSize/160,
                            texture: './img/brick.png'
                        }
                    }
                });
            case 'car':
                let color = Matter.Common.choose(['red', 'yellow', 'cyan']);
                return Matter.Bodies.rectangle(x, y, brickSize*2, brickSize, {
                    render: {
                        sprite: {
                            xScale: brickSize*2/318,
                            yScale: brickSize/159,
                            texture: `./img/${color}car.png`
                        }
                    },
                    chamfer: {
                        radius: brickSize/5
                    }
                });
            case 'ball':
                return Matter.Bodies.circle(x, y, brickSize/2, {
                    render: {
                        sprite: {
                            xScale: brickSize/302,
                            yScale: brickSize/302,
                            texture: './img/ball.png'
                        }
                    }
                });
            }
        });
        Matter.World.add(this.world, [this.stack]);
    }

    addBrickGC() {
        let maxy = this.height+this.brickSize*2;
        Matter.Events.on(this.engine, 'afterUpdate', e => {
            let cnt = 0;
            for (let brick of this.stack.bodies) {
                if (brick.position.y < maxy && brick.position.y > 0) {
                    cnt++;
                }
            }
            this.drawPoints(cnt);
        });
    }

    addBall(index) {
        let r = 100;
        this.ball[index] = Matter.Bodies.trapezoid(r + index*r*2, r, 50, 165, 0.5, {
            density: 0.0001,
            friction: 0,
            frictionAir: 0,
            frictionStatic: 0,
            restitution: 0,
            inertia: Infinity,
            render: {
                sprite: {
                    xScale: 0.5,
                    yScale: 0.5,
                    texture: './img/sweeper.png'
                }
            }
        });
        this.ballConstraint[index] = Matter.Constraint.create({
            pointA: { x: r + index*r*2, y: r },
            bodyB: this.ball[index],
            length: 0,
            stiffness: .2
        });
        Matter.World.add(this.world, [this.ball[index], this.ballConstraint[index]]);
    }

    drawPoints(points) {
        if (this.ended) {
            return;
        }
        if (points === 0) {
            this.stop();
            let now = new Date().getTime();
            this.points.innerHTML= ((now - this.startTime) / 1000.) + ' s';
        } else {
            this.points.innerHTML= points;
        }
    }

    setRight(x, y) {
        if (!this.startTime) {
            this.startTime = new Date().getTime();
        }
        console.log('setRight');
        this.ballConstraint[0].pointA = {x: x*this.width, y: y*this.height};
    }

    setLeft(x, y) {
        if (!this.startTime) {
            this.startTime = new Date().getTime();
        }
        console.log('setLeft');
        this.ballConstraint[1].pointA = {x: x*this.width, y: y*this.height};
    }
}
