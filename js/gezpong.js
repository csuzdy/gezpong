class GezPong {

    constructor(width, height, ballr) {
        console.log('itt2');
        this.engine = Matter.Engine.create();
        this.world = this.engine.world;
        this.world.bodies = [];
        this.world.gravity = {
            x: 0,
            y: 0
        };
        this.width = width;
        this.height = height;
        this.ballr = ballr;
        this.render = Matter.Render.create({
            element: document.body,
            engine: this.engine,
            options: {
                width: this.width,
                height: this.height,
                background: 'white',
                wireframes: false
            }
        });
        this.runner = Matter.Runner.create();
    }

    start() {
        console.log('Start app');
        this.addWalls();
        this.addBall();
        this.addRackets();
        this.attachKeyboardListener();
        this.attachBallRestarter();
        Matter.Render.run(this.render);
        Matter.Runner.run(this.runner, this.engine);
    }

    stop() {
        Matter.Render.stop(this.render);
        Matter.Runner.stop(this.runner);
    }

    addWalls() {
        Matter.World.add(this.world, [
            Matter.Bodies.rectangle(this.width/2, 5, this.width, 10, {isStatic: true}),
            Matter.Bodies.rectangle(this.width/2, this.height-5, this.width, 10, {isStatic: true})
        ]);
    }

    addBall() {
        this.ball = Matter.Bodies.circle(this.width/2, this.height/2, this.ballr, {
                density: 0.0001,
                friction: 0,
                frictionAir: 0,
                frictionStatic: 0,
                restitution: 1,
                inertia: Infinity,
                render: {
                    sprite: {
                        xScale: this.ballr*2/302.,
                        yScale: this.ballr*2/302.,
                        texture: './img/ball.png'
                    }
                }
            });
        Matter.Body.setVelocity(this.ball, {x: 5, y: 5});
        Matter.World.add(this.world, [this.ball]);
    }

    addRackets() {
        let racketsize = 150,
            racketw = 15;
        let options = {
            isStatic: true
        };
        this.racketLeft = Matter.Bodies.rectangle(racketw, this.height/2, racketw, racketsize, options);
        this.racketRight = Matter.Bodies.rectangle(this.width-racketw, this.height/2, racketw, racketsize, options);
        Matter.World.add(this.world, [this.racketLeft, this.racketRight]);
    }

    attachKeyboardListener() {
        document.addEventListener('keydown', e => {
            let dy = 20;
            switch (e.keyCode) {
            case 38:
                console.log('up');
                Matter.Body.translate(this.racketRight, {x:0, y: -dy});
                break;
            case 40:
                console.log('down');
                Matter.Body.translate(this.racketRight, {x:0, y: dy});
                break;
            case 87:
                console.log('w');
                Matter.Body.translate(this.racketLeft, {x:0, y: -dy});
                break;
            case 83:
                console.log('s');
                Matter.Body.translate(this.racketLeft, {x:0, y: dy});
                break;
            }
        });
        document.addEventListener('mousemove', e => {
            Matter.Body.setPosition(this.racketRight, {x: this.width-15, y: e.clientY});
            Matter.Body.setPosition(this.racketLeft, {x: 15, y: e.clientY});
        });
    }

    attachBallRestarter() {
        Matter.Events.on(this.engine, 'afterUpdate', () => {
            let x = this.ball.position.x;
            if (x < 0 || x > this.width) {
                Matter.Body.setPosition(this.ball, {x: this.width/2, y: this.height/2});
                Matter.Body.setVelocity(this.ball, {x: 5, y: 5});
            }

        });
    }

    setRight(x, y) {
        Matter.Body.setPosition(this.racketRight, {x: this.width-15, y: y*this.height});
    }

    setLeft(x, y) {
        Matter.Body.setPosition(this.racketLeft, {x: 15, y: y*this.height});
    }
}
