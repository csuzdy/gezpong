class MarkerDetector {

    constructor(game) {
        this.canvas = document.getElementById('canvas');
        this.context = canvas.getContext('2d');
        this.game = game;
        this.last = {};
        tracking.ColorTracker.registerColor('red', function(r, g, b) {
            let thresholdRatio = 1.5,
                threshold = 15;

            if ((r > Math.max(g,b)*thresholdRatio) && Math.abs(g-b) < threshold) {
                  return true;
            }
            return false;
        });
        tracking.ColorTracker.registerColor('green', function(r, g, b) {
            let thresholdRatio = 1.2,
                threshold = 15;

            if ((g > Math.max(r,b)*thresholdRatio) && Math.abs(r-b) < threshold) {
                  return true;
            }
            return false;
        });
        this.tracker = new tracking.ColorTracker(['green', 'red']);
        tracking.track('#video', this.tracker, {camera: true});
        this.xmax = this.canvas.width*0.9;
        this.xmin = this.canvas.width*0.1;
        this.ymax = this.canvas.height*0.9;
        this.ymin = this.canvas.height*0.1;
        this.clipwidth = this.canvas.width*0.8;
        this.clipheight = this.canvas.height*0.8;
        this.maxv = 0.1;
    }

    start() {
        this.tracker.on('track', event => {
            this.initCanvas();
            event.data.forEach(rect => {
                this.drawRect(rect);
                let [x,y] = this.clip(rect);
                let [correctx, correcty] = this.correct(x, y, rect.color);
                console.log(`found: ${correctx}, ${correcty}`);
                if (!this.game) {
                    return;
                }
                this.drawPoint(rect, rect.color);
                if (rect.color === 'red') {
                    this.game.setRight(correctx, correcty);
                } else if (rect.color === 'green') {
                    this.game.setLeft(correctx, correcty);
                }
            });
        });
    }

    initCanvas() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.strokeStyle = 'black';
        this.context.strokeRect(this.xmin, this.ymin, this.clipwidth, this.clipheight);
    }

    clip(rect) {
        let centerx = rect.x + rect.width/2,
            centery = rect.y + rect.height/2.;

        centerx = Math.max(centerx, this.xmin);
        centerx = Math.min(centerx, this.xmax);
        centery = Math.max(centery, this.ymin);
        centery = Math.min(centery, this.ymax);
        let actx = 1-((centerx-this.xmin) / this.clipwidth);
        let acty = (centery-this.ymin) / this.clipheight;
        return [actx, acty];
    }

    drawRect(rect) {
        this.context.strokeStyle = rect.color;
        this.context.strokeRect(rect.x, rect.y, rect.width, rect.height);
        this.context.font = '11px Helvetica';
        this.context.fillStyle = "#fff";
        this.context.fillText('x: ' + rect.x + 'px', rect.x + rect.width + 5, rect.y + 11);
        this.context.fillText('y: ' + rect.y + 'px', rect.x + rect.width + 5, rect.y + 22);
    }

    drawPoint(rect, color) {
        this.context.fillStyle = color;
        this.context.fillRect(rect.x, rect.y, rect.width, rect.height);
    }

    correct(x, y, color) {
        if (!this.last[color]) {
            this.last[color] = {x, y};
        }
        let ret = [(this.last[color].x + x)/2, (this.last[color].y + y) / 2];
        if (!ret[0]) {
            console.log(`arr: ${this.last[color].x}`);
        }
        this.last[color] = {x: ret[0], y: ret[1]};
        return ret;
    }

    getVelocity(last, act) {
        let dx = act.x - last.x,
            dy = act.y - last.y;
        return dx*dx + dy*dy;
    }
}
